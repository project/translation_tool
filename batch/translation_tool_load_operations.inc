<?php

/**
 * Operation node, get node and create a new node translated.
 *
 * @param $nodes
 *    Node load
 * @param $config_fields
 *    Fields to be translated
 * @param $handlers
 *    Handlers for execution function field
 * @param $language_target
 *    Language target for using translation
 */
function translation_tool_operation_nodes($nodes, $config_fields, $handlers, $language_target, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['index'] = 0;
    $context['sandbox']['last_node'] = count($nodes);
    $context['results']['node'] = array(
      'n_nodes' => count($nodes),
      'n_translated' => 0,
      'n_errors' => array()
    );
  }
  $node = $nodes[$context['sandbox']['index']];
  $context['sandbox']['index']++;

  $fields = $config_fields[$node->type];
  $node_load = node_load($node->nid);

  // Remove old references for create a new node.
  $node_translated = _translation_tool_prepare_node_for_translation($node_load);
  $node_translated->language = $language_target;

  try {
    // Create a new node translated.
    $error_in_translate = FALSE;
    $node_translated->tnid = $node->nid;
    node_save($node_translated);

    if (count($fields['fields']) > 0) {
      // Translate fields common.
      _translation_tool_process_translation_fields($node_translated, $fields['fields'], $handlers);
    }
    if (count($fields['field_collection']) > 0 && module_exists('field_collection')) {
      // Translate and save a new field collection item.
      _translation_tool_process_field_collection_items($node_translated, $fields['field_collection'], $handlers);
    }
    node_save($node_translated);
    $context['results']['node']['n_translated']++;
  }
  catch (TranslationToolFieldCollectionException $e) {
    $context['results']['node']['n_errors'][] = array(
      'error_msg' => $e->messageEndUser(),
      'error_code' => $e->getCode(),
      'current_node' => $node->nid,
    );
    $error_in_translate = TRUE;
  }
  catch (TranslationToolFieldException $e) {
    $context['results']['node']['n_errors'][] = array(
      'error_msg' => $e->messageEndUser(),
      'error_code' => $e->getCode(),
      'current_node' => $node->nid,
    );
    $error_in_translate = TRUE;
  }
  catch (Exception $e) {
    $context['results']['node']['n_errors'][] = array(
      'error_msg' => $e->getMessage(),
      'error_code' => $e->getCode(),
      'current_node' => $node->nid,
    );
    $error_in_translate = TRUE;
  }
  finally {
    if ($error_in_translate) {
      node_delete($node_translated->nid);
    }
    elseif (empty($node_load->tnid)) {
      $node_load->tnid = $node_load->nid;
      node_save($node_load);
    }
  }

  // Verify actual condition batch.
  $context['finished'] = 0;
  if ($context['sandbox']['index'] == $context['sandbox']['last_node']) {
    $context['finished'] = 1;
  }
  $context['message'] = t('Translate node !nid', array('!nid' => $node->nid));
}

/**
 * Operation menus, load menus and create a new menus translated.
 *
 * @param Array $menus
 *        List all menus to be translated.
 * @param $config
 *        Array config.
 * @param Array $context
 *        Context
 */
function translation_tool_operation_menus($menus, $config, &$context) {
  foreach ($menus as $menu_items) {
    _translation_tool_translate_menu_items($menu_items['links'], $config['target']);
  }
}
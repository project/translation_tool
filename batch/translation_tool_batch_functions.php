<?php

/**
 * Implments hook for preprocess collect nodes before to send for translation.
 */
function translation_tool_preprocess_collect_nodes_to_be_translateds(&$nodes) {
  drupal_alter(TRANSLATION_TOOL_HOOK_PREPROCESS_COLLECT_NODES, $nodes);
}

/**
 * Validate params for initiate execution batch.
 *
 * @param array $config_translation
 *        Array of congig translation.
 * @param array $config_fields_handlers
 *        Array of field handlers.
 *
 * @return array
 *        If found errors return array
 */
function translation_tool_validate_execution($config_translation, $config_fields_handlers) {
  if (
    count($config_translation['content_types']) <= 0 &&
    count($config_translation['menus']) <= 0 &&
    $config_translation['var_options_18n'] === 0 &&
    $config_translation['variable_translate_interface'] === 0
  ) {
    return array(
      'error' => TRUE,
      'error_msg' => 'Select at least one content to be translated!',
      'error_type' => 'error',
      'redirect_to' => 'admin/config/translation_tool',
    );
  }

  if ($config_fields_handlers === FALSE) {
    return array(
      'error' => TRUE,
      'error_msg' => 'To execute the translation it is necessary to choose the handler for fields first!',
      'error_type' => 'error',
      'redirect_to' => 'admin/config/translation_tool/fields',
    );
  }
  /*
   * Get callback for invoke api translated contents
   */
  $callback_translate = translation_tool_get_callback_translate();
  if (!function_exists($callback_translate)) {
    return array(
      'error' => TRUE,
      'error_msg' => 'callback for api translate not define!',
      'error_type' => 'error',
      'redirect_to' => 'admin/config/translation_tool/config_api_translate',
    );
  }
}

/**
 * Function for clone node and set new language for him.
 *
 * @param object $node
 *   Node.
 *
 * @return object array
 *   Return node without old values of node, vid, nid and path.
 */
function _translation_tool_prepare_node_for_translation($node) {
  $node_translated = clone $node;
  $unset_fields = array(
    'vid', 'nid',
  );
  foreach ($unset_fields as $ufield) {
    unset($node_translated->{$ufield});
  }

  // Copy path alias from original.
  $node_path = path_load(
    array('source' => 'node/' . $node->nid)
  );
  if (isset($node_translated->path) && isset($node_path['alias'])) {
    $node_translated->path['alias'] = $node_path['alias'];
  }

  return $node_translated;
}

/**
 * Function for count words per context in batch execution.
 *
 * @param $context
 * @param $text
 */
function _translation_tool_count_words_in_execution($text, $context = 'node') {
  switch ($context) {
    case 'node':
      $_SESSION['batch_words']['node'] += str_word_count($text);
      break;
    case 'menu':
      $_SESSION['batch_words']['menu'] += str_word_count($text);
      break;
    case 'variables':
      $_SESSION['batch_words']['variables'] += str_word_count($text);
      break;
    default:
      break;
  }
}

/**
 * Map fields with handler to access values in translations process.
 *
 * @return array
 */
function _translation_tool_map_fields_handler() {
  $config_fields_handlers = variable_get(TRANSLATION_TOOL_VAR_CONFIG_FIELDS_HANDLER, FALSE);
  $config_fields_handlers_map = array();
  foreach ($config_fields_handlers as $type => $fields) {
    foreach ($fields as $name_field => $handler) {
      $config_fields_handlers_map[$name_field] = $handler;
    }
  }
  return $config_fields_handlers_map;
}

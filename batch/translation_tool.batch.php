<?php

/**
 * Execute batch translation.
 */
function translation_tool_init_batch() {
  module_load_include("php", "translation_tool", "batch/translation_tool_batch_functions");
  $batch = array(
    'operations' => array(),
    'finished' => 'translation_tool_batch_fix_finished',
    'title' => t('Batch fix'),
    'init_message' => t('Fix is starting...'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Fix has encountered an error.'),
    'file' => drupal_get_path('module', 'translation_tool') . '/batch/translation_tool_load_operations.inc',
  );

  // Get params and validate params for initiate execution.
  $config_translation = unserialize(variable_get(TRANSLATION_TOOL_CONTENTS_TO_BE_TRANSLATED, array()));
  $config_fields_handlers = variable_get(TRANSLATION_TOOL_VAR_CONFIG_FIELDS_HANDLER, array());


  $validate = translation_tool_validate_execution($config_translation, $config_fields_handlers);
  if (isset($validate['error'])) {
    drupal_set_message($validate['error_msg'], $validate['error_type']);
    return drupal_goto($validate['redirect_to']);
  }

  /*
   * Initiate setup operation for nodes.
   */
  $config_content_types_to_be_translated = unserialize(variable_get(TRANSLATION_TOOL_CONFIGURE_CONTENTS_DATA, serialize(array())));
  $content_types_load = array_keys($config_translation['content_types']);
  $nodes = translation_tool_collect_nodes($content_types_load);
  if (count($nodes) > 0) {
    // Execute all hooks preprocess_collect_nodes implemented by modules.
    translation_tool_preprocess_collect_nodes_to_be_translateds($nodes);
    $batch['operations'][] = array(
      'translation_tool_operation_nodes',
      array(
        $nodes,
        $config_content_types_to_be_translated['content_types'],
        $config_fields_handlers, $config_translation['target'],
      ),
    );
  }
  /*
   * End setup operation nodes.
   */

  /*
   * Begin setup for operation variables
   */
  /*
   * End setup operation variables.
   */

  /*
   * Begin setup for operation menus
   */
  $menus = translation_tool_collect_menus(array_values($config_translation['menus']));
  if (count($menus) > 0) {
    $batch['operations'][] = array(
      'translation_tool_operation_menus',
      array(
        $menus,
        $config_translation,
      ),
    );
  }
  /*
   * End setup
   */

  batch_set($batch);
  // The path to redirect to when done.
  batch_process('admin/config/translation_tool/report-status-translation');
}

/**
 * Function execution end turn batch.
 */
function translation_tool_batch_fix_finished($success, $results, $operations) {
  if ($success) {
    $_SESSION['report'] = $results;

    if (isset($_SESSION['batch_words']['node'])) {
      $_SESSION['report']['node']['count_words'] = $_SESSION['batch_words']['node'];
    }

    if (isset($_SESSION['batch_words']['menu'])) {
      $_SESSION['report']['menu']['count_words'] = $_SESSION['batch_words']['menu'];
    }

    if (isset($_SESSION['batch_words']['variables'])) {
      $_SESSION['report']['variables']['count_words'] = $_SESSION['batch_words']['variables'];
    }
    drupal_set_message("Batch run is finished");
  }
  if (!empty($results['node']['n_errors'])) {
    $errors = array_column($results['node']['n_errors'], 'error_msg', 'current_node');
    watchdog('Translation Tool', print_r($errors, TRUE));
  }
}


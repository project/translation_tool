<?php

/**
 * Hook for translation content.
 *
 * @param string $text
 *        Text to be translated.
 */
function translation_tool_translation_content($text) {
}

/**
 * Hook form for save configuration of api.
 */
function translation_tool_translation_form_config_api() {
}
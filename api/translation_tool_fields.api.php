<?php

/**
 * Function for save text translated in field title.
 *
 * @param object $node
 *        Node for access field.
 */
function translation_tool_field_handle_title(&$node) {
  try {
    $text = translation_tool_execute_translation($node->title);
    $node->title = $text;
  }
  catch (Exception $e) {
    throw new Exception($e->getMessage(), $e->getCode());
  }
}

/**
 * Function for save text translated in field body.
 *
 * @param object $node
 *        Node for access field body.
 */
function translation_tool_field_handle_body(&$node) {
  try{
    $text = translation_tool_execute_translation($node->title);
    $node->body[LANGUAGE_NONE][0]['value'] = $text;
  }
  catch (Exception $e) {
    throw new Exception($e->getMessage(), $e->getCode());
  }
}

/**
 * Generic function,
 * for save text translated in node object when multiple values.
 *
 * @param object $node
 *        Node for access field.
 * @param string $field
 *        Name of field for using access node.
 */
function translation_tool_field_multi_values(&$node, $field) {
  foreach ($node->{$field}[LANGUAGE_NONE] as $index => $field_item) {
    try {
      $text = translation_tool_execute_translation($field_item['value']);
      $node->{$field}[LANGUAGE_NONE][$index]['value'] = $text;
    }
    catch (Exception $e) {
      throw new Exception($e->getMessage(), $e->getCode());
    }
  }
}

/**
 * Generic function for save value translated when unic value.
 *
 * @param object $node
 *        Node for access field.
 * @param string $field
 *        Name of field for access in node.
 *
 * @throws TranslationToolFieldException;
 */
function translation_tool_field_value(&$node, $field) {
  try {
    $text = translation_tool_execute_translation($node->{$field}[LANGUAGE_NONE][0]['value']);
    $node->{$field}[LANGUAGE_NONE][0]['value'] = $text;
  }
  catch (Exception $e) {
    throw new Exception($e->getMessage(), $e->getCode());
  }
}

/**
 * Implement hook field_handle_image.
 *
 * @param object $node
 *   Node to be translated.
 * @param $field
 *   Field to be translated.
 */
function translation_tool_field_handle_image(&$node, $field) {
  try {
    if (isset($node->{$field}[LANGUAGE_NONE][0]['alt']) && !empty($node->{$field}[LANGUAGE_NONE][0]['alt'])) {
      $node->{$field}[LANGUAGE_NONE][0]['alt'] = translation_tool_execute_translation($node->{$field}[LANGUAGE_NONE][0]['alt']);
    }

    if (isset($node->{$field}[LANGUAGE_NONE][0]['title']) && !empty($node->{$field}[LANGUAGE_NONE][0]['title'])) {
      $node->{$field}[LANGUAGE_NONE][0]['title'] = translation_tool_execute_translation($node->{$field}[LANGUAGE_NONE][0]['title']);
    }
  }
  catch (Exception $e) {
    throw new Exception($e->getMessage(), $e->getCode());
  }
}
<?php

/**
 * Function for load all links by name.
 *
 * @param $config
 *        Array of menu names.
 * @return array
 *        Array with menu parent and sub links loaded.
 */
function translation_tool_collect_menus($config){
  $menus = array();
  foreach ($config as $menu) {
    $load = menu_load($menu);
    $links = menu_load_links($load['menu_name']);

    // Filter only parent links.
    $parent_links = array_filter($links, function($link) {
      return $link['plid'] === "0";
    });

    $menus[] = array(
      'parent' => $load,
      'links' => $parent_links,
    );
  }
  return $menus;
}

/**
 * Translate all menu links and your child.
 *
 * @param array $menu_items
 *   Array of menu links to be translated.
 *
 * @param string $parent_translated
 *   Item menu translated.
 *
 * @param $language_target
 *   Language code to be translated.
 */
function _translation_tool_translate_menu_items($menu_items, $language_target, $parent_translated = NULL) {
  foreach ($menu_items as $link) {
    // Check menu has child to be translated.
    if ($link['has_children']) {

      // Translated parent link menu.
      $current_parent_translated = _translation_tool_translate_menu_link($link, $language_target, $parent_translated);

      // Load all child of menu.
      $load_children = _translation_tool_load_child_items($link['mlid']);
      _translation_tool_translate_menu_items($load_children, $language_target, $current_parent_translated);
    }
    else {
      // Execute translation of menu link.
      _translation_tool_translate_menu_link($link, $language_target, $parent_translated);
    }
  }
}

/**
 * Execute translation of meu links.
 *
 * @param array $link
 *   Array with menu link data.
 * @param string $language_target
 *   Language code to be translated.
 *
 * @return DatabaseStatementInterface|int|mixed
 */
function _translation_tool_translate_menu_link($link, $language_target, $parent_translated = NULL) {
  $link_translated = $link;
  unset($link_translated['mlid']);

  $text = translation_tool_execute_translation($link['link_title']);

  $link_translated['link_title'] = $text;
  $link_translated['language'] = $language_target;
  if ($parent_translated) {
    $link_translated['plid'] = $parent_translated;
  }

  return menu_link_save($link_translated);
}

/**
 * Get mlids from mlid father.
 *
 * @param string $mlid
 *   Mlid from menu link father.
 *
 * @return array
 *   Array of mlids.
 */
function _translation_tool_get_menu_child($mlid) {
  $select = db_select('menu_links', 'ml');

  $select->fields('ml', array('mlid'));
  $select->condition('ml.plid', $mlid, '=');

  return $select->execute()->fetchAll(PDO::FETCH_COLUMN);
}

/**
 * Load all child of parent link.
 *
 * @param string $mlid
 *   mlid of parent link menu.
 *
 * @return array
 *   Array of menu links loaded.
 */
function _translation_tool_load_child_items($mlid) {
  $mlids = _translation_tool_get_menu_child($mlid);
  $menu_links = array();
  foreach ($mlids as $mlid) {
    $menu_links[$mlid] = menu_link_load($mlid);
  }
  return $menu_links;
}


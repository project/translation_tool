<?php
/**
 * Created by PhpStorm.
 * User: joao
 * Date: 13/01/17
 * Time: 21:40
 */

/**
 * Hook collect nodes for translation.
 *
 * @param array $config
 *        config define by user.
 */
function translation_tool_collect_nodes($config) {
  if (empty($config)) {
    return array();
  }

  $select = db_select("node", "n")
              ->fields("n", array('type', 'nid', 'language'))
              ->condition('type', $config, "IN");

  $config = unserialize(variable_get(TRANSLATION_TOOL_CONTENTS_TO_BE_TRANSLATED));
  if (!empty($config['source'])) {
    $select->condition('language', $config['source'], '=');
  }
  return $select->execute()
                ->fetchAll();
}

/**
 * Hook for alter params for config before initialize collect nodes for translated.
 *
 * @param $config config define by user
 */
function translation_tool_preprocess_collect_nodes(&$nodes) {}

/**
 * Hook for get nodes or changing nodes before send to be translated.
 *
 * @param $config
 * @param $nodes
 */
function translation_tool_process_collect_nodes(&$config, &$nodes) {}

/**
 * Hook for save node transalted
 * @param $node node translated to be saved
 */
function translation_tool_save_node_translated($node){}
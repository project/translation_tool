(function($) {
  $(document).ready(function(){

    if ($('form#translation-tool-form-config-content-to-be-translated').length > 0) {
      var search_var_options = "";
      $('#edit-search-variable, #edit-search-translate-interface, #edit-search_variable_i18n').keyup(function(){
        search_var_options = $(this).val();
        switch ($(this).attr('id')){
          case 'edit-search-variable':
            context = 'variables';
            break;
          case 'edit-search-translate-interface':
            context = 'variable_translate_interface';
            break;
          case 'edit-search_variable_i18n':
            context = 'variable_store';
            break;
        }
        $('input[name^="'+context+'"]').each(function(indice, item){
          var label = $('label[for="'+item.id+'"]').text();
          if (item.value.indexOf(search_var_options) === -1 && label.indexOf(search_var_options) === -1) {
            $(this).parent().hide();
          }
          else {
            $(this).parent().show();
          }
        });
      });
    }
  });

})(jQuery);
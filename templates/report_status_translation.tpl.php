<?php
/**
 * @file
 * Template to show report status after translate.
 */
?>
<style>
  .report-item{
    width: 100%;
    height: 200px;
    border: 1px solid black;
    margin: 0px 20px 20px 0px;
    padding: 10px;
    overflow: auto;
  }
  .report-resume{
      width: 30%;
      position: relative;
      top: 0px;
      left: 0px;
      float: left;
  }
  .report-errors{
      width: 70%;
      right: 0px;
      top:0px;
      overflow: auto;
      max-height: 200px;
      position: relative;
      float: left;
  }
</style>
<h3>Report by last translation execute.</h3>
<?php if (isset($node)) { ?>
<div class="report-item">
    <div class="report-resume">
      <h2>Nodes translated</h2>
      <p>
        <?php print sprintf("Nodes translated: <b>%s</b> of <b>%s</b>", $node['n_translated'], $node['n_nodes']) ?>
      </p>
      <p>
        <?php print sprintf("Count words translated: <b>%s</b>", $node['count_words']) ?>
      </p>
      <p>
        <?php print sprintf("Exceptions during translation: <b>%s</b>", count($node['n_errors'])) ?>
      </p>
    </div>
    <div class="report-errors">
      <?php if (count($node['n_errors']) > 0) { ?>
      <table>
          <thead>
              <tr>
                  <th>Node</th>
                  <th>Error code</th>
                  <th>Message</th>
              </tr>
          </thead>
          <tbody>
          <?php foreach ($node['n_errors'] as $error) { ?>
              <tr>
                  <td><?php print $error['current_node'] ?></td>
                  <td><?php print $error['error_code'] ?></td>
                  <td><?php print $error['error_msg'] ?></td>
              </tr>
          <?php } ?>
          </tbody>
      </table>
      <?php } ?>
    </div>
</div>
<?php } ?>
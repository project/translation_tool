<?php

class TranslationToolFieldCollectionException extends TranslationToolFieldException {

  private $field_collection;

  public function __construct(TranslationToolFieldException $e, $node, $field, $field_collection)
  {
    $this->field_collection = $field_collection;
    parent::__construct($e, $node, $field);
  }

  public function messageEndUser() {
    return sprintf(
      "Error translating node %s on the %s item from field collection %s",
      $this->node,
      $this->field_collection,
      $this->field
    );
  }

  /**
   * @return mixed
   */
  public function getFieldCollection()
  {
    return $this->field_collection;
  }

  /**
   * @param mixed $field_collection
   */
  public function setFieldCollection($field_collection)
  {
    $this->field_collection = $field_collection;
  }
}

class TranslationToolFieldException extends Exception {

  protected $field;

  protected $node;

  public function __construct(Exception $e, $node, $field)
  {
    $this->node = $node;
    $this->field = $field;
    parent::__construct($e->getMessage(), $e->getCode());
  }

  public function messageEndUser() {
    if (!empty($this->message)) {
      return $this->message;
    }

    return sprintf(
      "Error translating node %s on the %s",
      $this->node,
      $this->field
    );
  }

  /**
   * @return Exception
   */
  public function getField()
  {
    return $this->field;
  }

  /**
   * @param Exception $field
   */
  public function setField(Exception $field)
  {
    $this->field = $field;
  }

  /**
   * @return int
   */
  public function getNode()
  {
    return $this->node;
  }

  /**
   * @param int $node
   */
  public function setNode($node)
  {
    $this->node = $node;
  }
}
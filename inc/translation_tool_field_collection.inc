<?php

/**
 * Get config field collections and split,
 * access field collection name and your items.
 *
 * @param array $fields
 *        Fields save on configuration for using to be translated.
 *
 * @return array
 *        Return array of fields separated by field_collection item.
 */
function _translation_tool_extract_config_field_collection($fields) {
  $field_collection_config = array();
  foreach ($fields as $name => $field) {
    $explode = explode('|', $field);
    $field_collection_config[$explode[0]][] = _translation_tool_get_type_of_field_collection_field($explode[1]) . '|' . $explode[1];
  }
  return $field_collection_config;
}

/**
 * Function process field collections items.
 *
 * @param object $node_translated
 *        Node to be translated.
 * @param array $fields
 *        Array of fields to be access and translated.
 * @param array $handlers
 *        Array of handlers using in field type.
 * @throws Exception
 */
function _translation_tool_process_field_collection_items($node_translated, $fields, $handlers) {
  $field_collections = _translation_tool_extract_config_field_collection($fields);
  foreach ($field_collections as $name_field => $field_items) {
    $load_field_collections = field_get_items('node', $node_translated, $name_field);
    foreach ($load_field_collections as $item_of_collection) {
      try {
        $entity = field_collection_field_get_entity($item_of_collection);
        _translation_tool_process_translation_fields($entity, $field_items, $handlers);
        $entity->save(TRUE);
      }
      catch (TranslationToolFieldException $field_exception) {
        throw new TranslationToolFieldCollectionException($field_exception, $node_translated->nid, $name_field, $field_exception->getField());
      }
      catch (Exception $e) {
        throw $e;
      }
    }
  }
}
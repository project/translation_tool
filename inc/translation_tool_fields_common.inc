<?php

/**
 * Function for process fields common.
 *
 * @param object $node_translated
 *        Node object.
 * @param array $fields
 *        Array of fields to be translated.
 * @param array $handlers
 *        Array of handlers.
 *
 * @throws TranslationToolFieldException
 */
function _translation_tool_process_translation_fields(&$node_translated, $fields, $handlers) {
  foreach ($fields as $field) {

    list($type, $field) = explode('|', $field);

    $handler = $handlers[$type][$field];
    try {
      if (!function_exists($handler)) {
        $message = sprintf("Function (%s) define by field %s don't exists", $handler, $field);
        throw new Exception($message);
      }
      $handler($node_translated, $field);
    }
    catch (Exception $e) {
      throw new TranslationToolFieldException($e, $node_translated->nid, $field);
    }
  }
}

/**
 * Get type of field based on name.
 *
 * @param $field_name
 *   Name of field
 *
 * @return string
 *   Return type of field.
 */
function _translation_tool_get_type_of_field_collection_field($field_name) {

  $select = db_select('field_config', 'fc');

  $select->fields('fc', array('type'));
  $select->condition('field_name', $field_name, '=');

  return $select->execute()->fetchField(0);
}

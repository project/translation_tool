<?php

/**
 * Get modules that imeplemented hook_translate_content.
 *
 * @return array
 *          Array of info modules.
 */
function translation_tool_get_apis_avaliables() {
  return module_implements(TRANSLATION_TOOL_HOOK_TRANSLATE_CONTENT);
}

/**
 * Function for get api.
 *
 * @return null|string
 *          Return null or function for translated content
 */
function translation_tool_get_api_translation_content() {
  $api = variable_get(TRANSLATION_TOOL_VAR_TRANSLATE_API, NULL);
  return $api;
}

/**
 * Remove values of chack box don't checked.
 *
 * @param array $checkboxes
 *        Array of checkboxes.
 *
 * @return array
 *        Return array checkboxes checked.
 */
function _translation_tool_sanitize_checkboxes($checkboxes) {
  $values = array();
  foreach ($checkboxes as $name => $check) {
    if ($check !== 0) {
      $values[$name] = $check;
    }
  }
  return $values;
}

/**
 * Get values of variable_store.
 *
 * @return array
 *         Return array with values in variable_store table.
 */
function _translation_tool_get_variables_18n() {
  if (!module_exists('variable_store')) {
    return array();
  }
  $select = db_select('variable_store', 'v');
  $select->fields('v', array('name', 'serialized', 'value'));

  $select->condition('name', '', '<>');
  $select->condition('name', '#%', 'not like');
  $select->condition('name', '0', '<>');

  $list_variables = $select->execute()->fetchAll();

  $options = array();
  foreach ($list_variables as $variable) {
    if (_translation_tool_variable_value_is_translable($variable) && !empty($variable->name)) {
      $name = $variable->name;
      if (is_numeric($name)) {
        $name = $variable->value;
      }
      $options[$variable->name] = $name;
    }
  }
  return $options;
}

/**
 * Get values of locales_source.
 *
 * @return array
 *        Return array with contexts and options values.
 */
function _translation_tool_get_variables_t_interface() {
  if (!module_exists('locales')) {
    return array(
      'contexts' => array(),
      'options' => array(),
    );
  }
  $result = db_select('locales_source', 'ls')
              ->fields('ls', array('lid', 'context', 'source'))
              ->execute()
              ->fetchAll();

  $options = array();
  $contexts = array();
  foreach ($result as $item) {
    $options[$item->lid] = $item->source;
    $contexts[$item->context] = $item->context;
  }
  return array(
    'contexts' => $contexts,
    'options' => $options,
  );
}

/**
 * Function for check is variable is valid(the values its number).
 *
 * @return bool
 *        Return TRUE if value is translated.
 */
function _translation_tool_variable_value_is_translable($variable) {
  $value = $variable->value;
  if ($variable->serialized == 1) {
    $value = unserialize($variable->value);
    if (isset($value['value'])) {
      $value = $value['value'];
    }
  }
  if (!is_numeric($value) && !empty($value) && !is_bool($value)) {
    return TRUE;
  }
  return FALSE;
}
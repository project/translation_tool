<?php

/**
 * Form handle_fields.
 *
 * @return array
 *         Return the form for select handler per field type
 */
function translation_tool_form_handle_fields() {
  $form = array();
  $type_fields = array_merge(array('title' => 'title', 'body' => 'body'), translation_tool_get_fields());
  $default_values = variable_get(TRANSLATION_TOOL_VAR_CONFIG_FIELDS_HANDLER, array());
  if (count($default_values) === 0){
    drupal_set_message("Please, select handler for fields", 'warning');
  }
  foreach ($type_fields as $name => $field) {

    $handler_selected = (!empty($default_values[$name])) ? reset($default_values[$name]) : NULL;

    $form[$name] = array(
      '#type' => 'fieldset',
      '#title' => 'Fields type ' . $name,
      '#tree' => TRUE,
    );
    
    $form[$name]['handler'] = array(
      '#type' => 'select',
      '#title' => t('Select handle for field: ' . $field),
      '#options' => translation_tool_get_handlers_field($name),
      '#default_value' => $handler_selected,
    );

    $form[$name]['fields'] = array(
      '#type' => 'hidden',
      '#value' => $field,
    );
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  return $form;
}

/**
 * Implements form submit form handler_fields.
 *
 * @param array $form
 *        Array of fields form.
 * @param array $form_state
 *        Array of form_state.
 */
function translation_tool_form_handle_fields_submit($form, &$form_state) {
  $config_fields_handlers = array();
  $fields = $form_state['values'];

  // Sanitize values for fields config.
  foreach (array('submit', 'op', 'form_build_id', 'form_token', 'form_id') as $input) {
    unset($fields[$input]);
  }

  foreach ($fields as $type_of_field => $fields) {

    $extract_fields = explode(',', $fields['fields']);
    $handler = $fields['handler'];

    if (count($extract_fields) > 1) {
      foreach ($extract_fields as $field) {
        $config_fields_handlers[$type_of_field][$field] = $handler;
      }
    }
    else {
      $config_fields_handlers[$type_of_field][$fields['fields']] = $handler;
    }
  }
  $name = TRANSLATION_TOOL_VAR_CONFIG_FIELDS_HANDLER;
  variable_set($name, $config_fields_handlers);
  drupal_set_message("Configuration for fields handlers has ben saved with success!");
}


/**
 * Form for configure APIs.
 *
 * @return array
 *        Return the form for configuration
 *        all api avaliables for use in translated content
 */
function translation_tool_form_config_api() {
  $form = array();

  $form['general_config'] = array(
    '#type' => 'fieldset',
    '#title' => 'Test API translate',
    '#description' => 'Select the API for usily translate content',
  );

  $form['general_config']['api_enable'] = array(
    '#type' => 'select',
    '#options' => _translation_tool_get_apis_available_list(),
  );

  $form['general_config']['text_translate'] = array(
    '#type' => 'textfield',
    '#title' => t('Put the text for translated'),
    '#default_value' => "",
    '#size' => 60,
    '#maxlength' => 128,
  );

  $form['general_config']['text_translated'] = array(
    '#markup' => "
      <p id='text-translated'></p>
      <br>
     ",
  );

  $form['general_config']['test_connection'] = array(
    '#type' => 'button',
    '#value' => 'Test translate',
    '#ajax' => array(
      'callback' => 'translation_tool_test_translation_content',
      'wrapper' => 'text-translated',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  return $form;
}

/**
 * Implements form submit config API.
 *
 * @param array $form
 *        Array of fields.
 * @param array $form_state
 *        Array of form state.
 */
function translation_tool_form_config_api_submit($form, &$form_state) {
  if (!empty($form_state['values']['api_enable'])) {
    variable_set(TRANSLATION_TOOL_VAR_TRANSLATE_API, $form_state['values']['api_enable']);
  }
}

/**
 * Implements _translation_tool_get_variables_to_translate().
 *
 * @return array
 *         Return all variables
 */
function _translation_tool_get_variables_to_translate() {
  $select = db_select('variable_store', 'v');

  $select->condition('name', '', '<>');
  $select->condition('name', '#%', 'not like');
  $select->condition('name', '0', '<>');

  $select->fields('v', array('name', 'serialized', 'value'));

  $list_variables = $select->execute()->fetchAll();

  $options = array();
  foreach ($list_variables as $variable) {
    if (_variable_value_is_valid($variable)) {
      $name = $variable->name;
      if (is_numeric($name)) {
        $name = $variable->value;
      }
      $options[$variable->name] = $name;
    }
  }
  return $options;
}

/**
 * Form for config menus translate.
 *
 * @return array
 *        Array of form fields.
 */
function translation_tool_form_menu_config() {
  $form = array();
  $list_all_menus = menu_load_all();
  $options = array();
  $general_default_values = unserialize(variable_get(TRANSLATION_TOOL_CONFIGURE_CONTENTS_DATA, serialize(array())));
  $default_values = array();
  foreach ($list_all_menus as $name => $menu) {
    $options[$name] = sprintf("%s - %s", $menu['title'], $menu['description']);
    if (isset($general_default_values['menus'][$name]) && $general_default_values['menus'][$name] !== 0) {
      $default_values[] = $name;
    }
  }
  $form['menus'] = array(
    '#title' => t('Select menus to be translated'),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => $default_values,
  );
  return $form;
}

/**
 * Form to List fields for each content-type.
 */
function translation_tool_list_fields_per_content_type() {
  $form = array();
  $content_types_list = node_type_get_types();

  $general_default_values = unserialize(variable_get(TRANSLATION_TOOL_CONFIGURE_CONTENTS_DATA, serialize(array())));

  $form['group_content_types']['content_types'] = array(
    '#tree' => TRUE,
  );

  foreach ($content_types_list as $content_type) {
    $list_default_values = array();
    $list_fields = array();

    $fields = field_info_instances("node", $content_type->type);

    $list_fields['title|title'] = 'Title (title)';
    if (isset($general_default_values['content_types'][$content_type->type]['fields']['title|title'])) {
      $list_default_values[] = 'title|title';
    }

    $list_field_collections = array();
    $list_field_collections_default_values = array();

    foreach ($fields as $name_field => $value) {
      // Looking for Field Collections fields.
      if ($value['widget']['type'] == 'field_collection_embed') {
        $field_collection = field_info_instances('field_collection_item', $value['field_name']);
        foreach ($field_collection as $keyfc => $valuefc) {
          $key_fc = $name_field . '|' . $keyfc;
          $list_field_collections[$key_fc] = $valuefc['label'] . ' (' . $keyfc . ') <strong>[Field Collection from: ' . $name_field . ']</strong>';
          if (isset($general_default_values['content_types'][$content_type->type]['field_collection'][$key_fc])) {
            $list_field_collections_default_values[] = $key_fc;
          }
        }
      }
      else {
        $field_info = field_info_field($name_field);
        $group_field_by_type = $field_info['type'] . '|' .  $name_field;
        $list_fields[$group_field_by_type] = $value['label'] . ' (' . $name_field . ')';

        if (isset($general_default_values['content_types'][$content_type->type]['fields'][$group_field_by_type])) {
          $list_default_values[] = $group_field_by_type;
        }
      }
    }
    // Remove field tags.
    unset($list_fields['field_tags']);

    $form['group_content_types']['content_types'][$content_type->type] = array(
      '#type' => 'fieldset',
      '#title' => $content_type->name,
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    );

    $form['group_content_types']['content_types'][$content_type->type]['fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Select fields to be translated'),
      '#options' => $list_fields,
      '#default_value' => $list_default_values,
    );

    // Create checkbox if has field collection.
    if (module_exists('field_collection') && !empty($list_field_collections)) {
      $form['group_content_types']['content_types'][$content_type->type]['field_collection'] = array(
        '#type'          => 'checkboxes',
        '#title'         => t('Select fields on "field collection" to be translated'),
        '#options'       => $list_field_collections,
        '#default_value' => $list_field_collections_default_values,
      );
    }
  }

  return $form;
}

/**
 * Form for save configuration all content to bel translated.
 */
function translation_tool_form_config_content_to_be_translated(){
  $form = array();

  $form['form'] = array(
    '#type' => 'fieldset',
    '#title' => 'Check everything you want translated',
    '#description' => 'Select fields in content types and check menu / variables to be translated',
  );

  $form['form']['group_ct'] = array(
    '#type' => 'fieldset',
    '#title' => 'Content type available for translation',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $form['form']['group_ct']['content_types'] = translation_tool_list_fields_per_content_type();

  $form['form']['group_menus'] = array(
    '#type' => 'fieldset',
    '#title' => 'Menus avaliables for translation',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $form['form']['group_menus']['menus'] = translation_tool_form_menu_config();

  $form['form']['group_variables'] = array(
    '#type' => 'fieldset',
    '#title' => 'Variables avaliables for translation',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $vars_default_value = unserialize(variable_get(TRANSLATION_TOOL_CONFIGURE_CONTENTS_DATA, serialize(array())));

  $form['form']['group_variables']['variable_store'] = array(
    '#type' => 'fieldset',
    '#title' => 'Variables Locale 18n',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $var_18n_options = _translation_tool_get_variables_18n();

  if (count($var_18n_options) > 0) {
    $form['form']['group_variables']['variable_store']['search_variable_i18n'] = array(
      '#type' => 'textfield',
      '#attributes' => array(
        'placeholder' => 'Enter with name',
      ),
      '#title' => 'Search by name',
    );

    $form['form']['group_variables']['variable_store']['var_options_18n'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Check all variables that you want translated',
      '#options' => $var_18n_options,
      '#default_value' => isset($vars_default_value['var_options_18n']) ? $vars_default_value['var_options_18n'] : array(),
    );
  }
  else {
    $form['form']['group_variables']['variable_store']['var_options_18n'] = array(
      '#markup' => 'No variables available or module Variable Store not installed!',
    );
  }

  $form['form']['group_variables']['variable_translate_interface'] = array(
    '#type' => 'fieldset',
    '#title' => 'Variables Translate Interface',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
  );

  $variables_ti = _translation_tool_get_variables_t_interface();
  if (count($variables_ti['options']) > 0) {
    $form['form']['group_variables']['variable_translate_interface']['search_translate_interface'] = array(
      '#type' => 'textfield',
      '#attributes' => array(
        'placeholder' => 'Enter with name',
      ),
      '#title' => 'Search by name',
    );
    if (count($variables_ti['contexts']) > 1) {
      $form['form']['group_variables']['variable_translate_interface']['filter_translate_interface'] = array(
        '#type' => 'select',
        '#options' => $variables_ti['contexts'],
        '#title' => 'Filter by context',
      );
    }
    $form['form']['group_variables']['variable_translate_interface']['variable_translate_interface'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Check all variables that you want translated',
      '#options' => $variables_ti['options'],
      '#default_value' => $vars_default_value['variable_translate_interface'],
    );
  }
  else {
    $form['form']['group_variables']['variable_translate_interface']['var_options_18n'] = array(
      '#markup' => 'No variables available or module Variable Store not installed!',
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  return $form;
}

/**
 * Implements form_submit.
 *
 * @param array $form
 *        Array of fields.
 * @param array $form_state
 *        Array of form_state.
 */
function translation_tool_form_config_content_to_be_translated_submit(&$form, &$form_state) {
  $array_config = array();

  if (isset($form_state['values']['menus'])) {
    $array_config['menus'] = _translation_tool_sanitize_checkboxes($form_state['values']['menus']);
  }

  if (isset($form_state['values']['var_options_18n'])) {
    $array_config['var_options_18n'] = _translation_tool_sanitize_checkboxes($form_state['values']['var_options_18n']);
  }

  if (isset($form_state['values']['variable_translate_interface'])) {
    $array_config['variable_translate_interface'] = _translation_tool_sanitize_checkboxes($form_state['values']['variable_translate_interface']);
  }

  foreach ($form_state['values']['content_types'] as $type => $fields) {
    $array_config['content_types'][$type]['fields'] = _translation_tool_sanitize_checkboxes($fields['fields']);
    if (isset($fields['field_collection'])) {
      $array_config['content_types'][$type]['field_collection'] = _translation_tool_sanitize_checkboxes($fields['field_collection']);
    }
  }

  variable_set(TRANSLATION_TOOL_CONFIGURE_CONTENTS_DATA, serialize($array_config));
  drupal_set_message("Config has been saved with success!");
}

/**
 * Form execute translation.
 *
 * @param array $form
 *        Array form fields.
 * @param array $form_state
 *        Array form fields state.
 *
 * @return array
 *        Array of fields
 */
function translation_tool_form_execution_translate($form, $form_state){

  $default_values = array(
    'target' => 0,
    'api' => 0,
    'source' => 0,
    'content_types' => array(),
    'menus' => array(),
    'variable_translate_interface' => 0,
    'var_options_18n' => 0
  );

  $default_values = unserialize(variable_get(TRANSLATION_TOOL_CONTENTS_TO_BE_TRANSLATED, serialize($default_values)));

  $list_content_types = array();
  foreach (node_type_get_types() as $type) {
    $list_content_types[$type->type] = $type->name . " - " . $type->description;
  }

  $list_all_menus = menu_load_all();
  $list_menus_options = array();
  foreach ($list_all_menus as $name => $menu) {
    $list_menus_options[$name] = sprintf("%s - %s", $menu['title'], $menu['description']);
  }

  $all_languages = language_list();
  $languages_options = array();
  foreach ($all_languages as $locale => $language) {
    $languages_options[$locale] = $language->name;
  }

  // This is the field fieldset.
  $form['execute_params'] = array(
    '#type' => 'fieldset',
    '#title' => t('Configure your content to be translated.'),
  );

  $form['execute_params']['api'] = array(
    '#type' => 'select',
    '#options' => _translation_tool_get_apis_available_list(),
    '#requried' => TRUE,
    '#title' => 'API Avaliables.',
    '#description' => t('API avaliables for execute translation.'),
    '#default_value' => $default_values['api'],
  );

  $form['execute_params']['target'] = array(
    '#type' => 'select',
    '#options' => $languages_options,
    '#requried' => TRUE,
    '#title' => 'Select target',
    '#description' => t('Select target language.'),
    '#default_value' => $default_values['target'],
  );

  $form['execute_params']['source'] = array(
    '#type' => 'select',
    '#options' => $languages_options,
    '#requried' => TRUE,
    '#title' => 'Select source.',
    '#description' => t('Select source language.'),
    '#default_value' => $default_values['source'],
  );

  $form['execute_params']['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select your content types for translation'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  $form['execute_params']['content_types']['list_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Check content types',
    '#options' => $list_content_types,
    '#default_value' => $default_values['content_types'],
  );

  $form['execute_params']['menus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select menus for translation'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  $form['execute_params']['menus']['menu_options'] = array(
    '#type' => 'checkboxes',
    '#title' => 'List menus',
    '#options' => $list_menus_options,
    '#default_value' => $default_values['menus'],
  );

  $form['execute_params']['variables'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select variables for translation'),
    '#collapsible' => FALSE,
  );

  $form['execute_params']['variables']['var_options_18n'] = array(
    '#type' => 'checkbox',
    '#title' => 'Translate Variables 18n?',
    '#default_value' => $default_values['var_options_18n'],
  );

  $form['execute_params']['variables']['variable_translate_interface'] = array(
    '#type' => 'checkbox',
    '#title' => 'Translate Interface?',
    '#default_value' => $default_values['variable_translate_interface'],
  );

  $form['save_config'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#submit' => array('_translation_tool_save_config_for_execution'),
  );

  $form['save_and_translate'] = array(
    '#type' => 'submit',
    '#value' => t('Start translation'),
    '#submit' => array('_translation_tool_save_config_and_execute'),
    '#disabled' => !_translation_tool_api_enabled(),
  );

  return $form;
}

/**
 * Submit save config for execution translation.
 *
 * @param array $form
 *        Array form fields.
 * @param $form_state
 *        Array form state.
 */
function _translation_tool_save_config_for_execution($form, &$form_state) {
  $array_config = array(
    'api' => $form_state['values']['api'],
    'target' => $form_state['values']['target'],
    'source' => $form_state['values']['source'],
    'content_types' => _translation_tool_sanitize_checkboxes($form_state['values']['list_content_types']),
    'menus' => _translation_tool_sanitize_checkboxes($form_state['values']['menu_options']),
    'var_options_18n' => $form_state['values']['var_options_18n'],
    'variable_translate_interface' => $form_state['values']['variable_translate_interface'],
  );
  unset($_SESSION['config_google_api']);
  variable_set(TRANSLATION_TOOL_VAR_TRANSLATE_API, $form_state['values']['api']);
  variable_set(TRANSLATION_TOOL_CONTENTS_TO_BE_TRANSLATED, serialize($array_config));
  drupal_set_message("Configuration has been saved with success!");
}

/**
 * Method submit save config and initiate execution batch.
 *
 * @param array $form
 *        Array of fields.
 * @param array $form_state
 *        Array form fields state.
 */
function _translation_tool_save_config_and_execute($form, &$form_state) {
  _translation_tool_save_config_for_execution($form, $form_state);
  $form_state['redirect'] = 'admin/config/translation_tool/init_translation';
}